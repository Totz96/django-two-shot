from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login


# Create your views here.

# Function View
    # If request is a form
        # Create form
        # If the form is valid, save user
        # Login the user w/ login() function
        # Redirect to "home"
    # Otherwise request is not POST
        # Go to user creation instead
    # return render()
    
def signup(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserCreationForm()
        
    return render(request, 'registration/signup.html', {'form': form})