from django.shortcuts import render
from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.urls import reverse_lazy
from receipts.models import ExpenseCategory, Account, Receipt

USER_MODEL = settings.AUTH_USER_MODEL


# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/receipt_list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/receipt_create.html"
    fields = ["vendor", "total", "tax", "date", "category", "account"]
    success_url = reverse_lazy("home")

    def form_valid(self, form):
        form.instance.purchaser = self.request.user
        return super().form_valid(form)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/account_list.html"
    
    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/expense_list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/expense_create.html"
    fields = ["name"]

    def form_valid(self, form):
        expensecategory = form.save(commit=False)
        expensecategory.owner = self.request.user
        expensecategory.save()
        return redirect("list_expensecategory")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/account_create.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        account.save()
        return redirect("list_accounts")