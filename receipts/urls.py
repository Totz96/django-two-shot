from django.urls import path


from receipts.views import (
    AccountListView,
    AccountCreateView,
    ExpenseCategoryListView,
    ExpenseCategoryCreateView,
    ReceiptCreateView,
    ReceiptListView,
)



urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path("categories/", ExpenseCategoryListView.as_view(), name="list_expensecategory"),
    path("categories/create/", ExpenseCategoryCreateView.as_view(), name="create_expensecategory"),
    path("accounts/", AccountListView.as_view(), name="list_accounts"),
    path("accounts/create/", AccountCreateView.as_view(), name="create_account"),
]